# Group Service

The group service is responsible for keeping track of the members of a group as well as the permissions of the individual users.

## Documentation

Production: https://connect.kominal.com/group-service/api-docs
Test: https://connect-test.kominal.com/group-service/api-docs
