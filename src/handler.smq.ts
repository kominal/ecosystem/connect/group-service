import { Packet } from '@kominal/service-util/model/packet';
import { info, error } from '@kominal/service-util/helper/log';

export class SMQHandler {
	async onConnect(): Promise<void> {
		info('Connected to SwarmMQ cluster.');
	}

	async onDisconnect(): Promise<void> {
		error('Lost connection to SwarmMQ cluster.');
	}

	async onMessage(packet: Packet): Promise<void> {}
}
