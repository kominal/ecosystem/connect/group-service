import axios from 'axios';
import { STACK_NAME, SERVICE_TOKEN, TIMEOUT } from '@kominal/service-util/helper/environment';
import GroupDatabase from '@kominal/connect-models/group/group.database';
import { Document } from 'mongoose';
import MembershipDatabase from '@kominal/connect-models/membership/membership.database';
import { error } from '@kominal/service-util/helper/log';
import { ProfileEncrypted } from '@kominal/connect-models/profile/profile.encrypted';

export async function getGroup(groupId: string): Promise<Document> {
	const group = await GroupDatabase.findById(groupId);

	if (!group) {
		throw 'error.group.notfound';
	}

	return group;
}

export async function getMembership(groupId: string, userId: string) {
	const membership = await MembershipDatabase.findOne({ userId, groupId });

	if (!membership) {
		throw 'error.group.notmember';
	}

	return membership;
}

export async function verifyProfileOwnership(userId: string, profileId: string): Promise<boolean> {
	try {
		const response = await axios.get<ProfileEncrypted>(`http://${STACK_NAME}_profile-service:3000/${profileId}`, {
			headers: {
				authorization: SERVICE_TOKEN,
				userId,
			},
			timeout: TIMEOUT,
		});
		if (response.data && response.status == 200) {
			return response.data.active === true && response.data.userId === userId;
		}
	} catch (e) {
		error(e);
	}
	return false;
}
