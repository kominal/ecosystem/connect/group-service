import { verifyParameter } from '@kominal/service-util/helper/util';
import Router from '@kominal/service-util/helper/router';
import GroupDatabase from '@kominal/connect-models/group/group.database';

const router = new Router();

/**
 * Sets the last activity time for a group
 * @group Private
 * @security JWT
 * @route GET /activity/{groupId}
 * @consumes application/json
 * @produces application/json
 * @param {string} groupId.path.required - The group id of the group
 * @returns {void} 200 - The time was updated
 * @returns {Error}  401 - Unauthorized - Missing authorization header
 * @returns {Error}  406 - Not Acceptable - A parameter is missing or is invalid or the request could not be processed
 * @returns {Error}  500 - Internal Server Error
 */
router.postAsRoot('/activity/:groupId', async (req, res) => {
	const { groupId } = req.params;
	verifyParameter(groupId);

	await GroupDatabase.updateOne({ _id: groupId }, { lastActivity: new Date() });

	res.status(200).send();
});

export default router.getExpressRouter();
