import { verifyParameter } from '@kominal/service-util/helper/util';
import MembershipDatabase from '@kominal/connect-models/membership/membership.database';
import Router from '@kominal/service-util/helper/router';
import { getGroup, getMembership, verifyProfileOwnership } from '../helper';
import service from '..';

const router = new Router();

/**
 * Adds a user to a group
 * @group Protected
 * @security JWT
 * @route POST /add
 * @consumes application/json
 * @produces application/json
 * @param {string} groupId.body.required - The id of the group the user should be added to
 * @param {string} partnerId.body.required - The id of the user that should be added
 * @param {string} groupKey.body.required - The groupKey encrypted with the public key of the user
 * @param {string} displayname.body.required - The displayname encrypted with the groupKey
 * @returns {void} 200 - The user was added to the group
 * @returns {Error}  401 - Unauthorized - Missing authorization header
 * @returns {Error}  406 - Not Acceptable - A parameter is missing or is invalid or the request could not be processed
 * @returns {Error}  500 - Internal Server Error
 */
router.postAsUser('/add', async (req, res, userId) => {
	const { groupId, groupKey, profileId, profileKey, partnerId } = req.body;
	verifyParameter(groupId, partnerId, profileId, groupKey, profileKey.data, profileKey.iv);

	const group = await getGroup(groupId);

	if (group.get('type') === 'DM') {
		throw 'error.group.cannotmodifydm';
	}

	const membership = await getMembership(groupId, userId);

	if (membership.get('role') !== 'ADMIN') {
		throw 'error.group.notadmin';
	}

	if ((await MembershipDatabase.findOne({ groupId, userId: partnerId, left: null, removed: null })) != null) {
		throw 'error.group.alreadyamember';
	}

	if (!(await verifyProfileOwnership(partnerId, profileId))) {
		throw 'error.profile.ownership';
	}

	const membershipPartner = await MembershipDatabase.findOne({ groupId, userId: partnerId });

	if (membershipPartner) {
		await MembershipDatabase.updateMany(
			{ groupId, userId: partnerId },
			{ left: null, removed: null, profileId, profileKey: profileKey.data, profileKeyIv: profileKey.iv, lastUpdated: new Date() }
		);
	} else {
		await MembershipDatabase.create({
			groupId: group._id,
			userId: partnerId,
			profileId,
			profileKey: profileKey.data,
			profileKeyIv: profileKey.iv,
			joined: new Date(),
			left: null,
			removed: null,
			role: 'MEMBER',
			groupKey,
			lastUpdated: new Date(),
		});
	}

	service.getSMQClient()?.publish('TOPIC', `INTERNAL.REFRESH.${partnerId}`, 'REFRESH');
	service.getSMQClient()?.publish('TOPIC', `DIRECT.USER.${partnerId}`, 'GROUP.JOINED', { userId: partnerId });
	service.getSMQClient()?.publish('TOPIC', `DIRECT.GROUP.${groupId}`, 'GROUP.JOINED', { userId: partnerId });

	res.status(200).send();
});

export default router.getExpressRouter();
