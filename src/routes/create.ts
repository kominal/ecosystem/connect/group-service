import { verifyParameter } from '@kominal/service-util/helper/util';
import GroupDatabase from '@kominal/connect-models/group/group.database';
import Router from '@kominal/service-util/helper/router';
import MembershipDatabase from '@kominal/connect-models/membership/membership.database';
import { verifyProfileOwnership } from '../helper';
import service from '..';

const router = new Router();

/**
 * Creates a new group.
 * @group Protected
 * @security JWT
 * @route POST /
 * @consumes application/json
 * @produces application/json
 * @param {string} type.body.required - The type of the group (DM or GROUP)
 * @param {string} groupKey.body.required - The groupKey encrypted with the users public key
 * @param {string} displayname.body.required - The displayname encrypted with the groupKey of the user
 * @param {string} partnerId.body - Required if type is DM: The userId of the partner
 * @param {string} partnerGroupKey.body - Required if type is DM: The groupKey encrypted with the public key of the partner
 * @param {string} partnerDisplayname.body - Required if type is DM: The displayname of the partner encrypted with the groupKey
 * @param {string} name.body - Required if type is GROUP: The name of the group encrypted with the groupKey
 * @returns {void} 200 - The group was created
 * @returns {Error}  401 - Unauthorized - Missing authorization header
 * @returns {Error}  406 - Not Acceptable - A parameter is missing or is invalid or the request could not be processed
 * @returns {Error}  500 - Internal Server Error
 */
router.postAsUser('/', async (req, res, userId) => {
	const body = req.body;

	if (body.type === 'DM') {
		const { groupKey, profileId, profileKey, partnerId, partnerGroupKey, partnerProfileId, partnerProfileKey } = body;
		verifyParameter(
			groupKey,
			profileId,
			profileKey.data,
			profileKey.iv,
			partnerId,
			partnerGroupKey,
			partnerProfileId,
			partnerProfileKey.data,
			partnerProfileKey.iv
		);

		if (userId === partnerId) {
			throw 'error.dm.yourself';
		}

		const memberships = await MembershipDatabase.find({ userId });
		for (const membership of memberships) {
			const group = await GroupDatabase.findOne({ type: 'DM', _id: membership.get('groupId') });
			if (group == null) {
				continue;
			}

			const groupMembers = await MembershipDatabase.find({ groupId: group._id });
			if (groupMembers.length != 2) {
				continue;
			}

			const groupUserIds = groupMembers.map((groupMember) => String(groupMember.get('userId')));

			if (groupUserIds.includes(userId) && groupUserIds.includes(partnerId)) {
				const result = await MembershipDatabase.updateMany({ groupId: group._id }, { left: null, removed: null });
				res.status(200).send(group._id);
				if (result.modifiedCount > 0) {
					service.getSMQClient()?.publish('TOPIC', `INTERNAL.REFRESH.${userId}`, 'REFRESH');
					service.getSMQClient()?.publish('TOPIC', `INTERNAL.REFRESH.${partnerId}`, 'REFRESH');

					service.getSMQClient()?.publish('TOPIC', `DIRECT.USER.${userId}`, 'GROUP.JOINED', { userId });
					service.getSMQClient()?.publish('TOPIC', `DIRECT.USER.${userId}`, 'GROUP.JOINED', { userId: partnerId });
					service.getSMQClient()?.publish('TOPIC', `DIRECT.USER.${partnerId}`, 'GROUP.JOINED', { userId });
					service.getSMQClient()?.publish('TOPIC', `DIRECT.USER.${partnerId}`, 'GROUP.JOINED', { userId: partnerId });
				}
				return;
			}
		}

		if (!(await verifyProfileOwnership(userId, profileId))) {
			throw 'error.profile.ownership';
		}

		if (!(await verifyProfileOwnership(partnerId, partnerProfileId))) {
			throw 'error.profile.ownership';
		}

		const group = await GroupDatabase.create({
			type: 'DM',
		});

		await MembershipDatabase.insertMany([
			{
				groupId: group._id,
				userId,
				profileId,
				profileKey: profileKey.data,
				profileKeyIv: profileKey.iv,
				joined: new Date(),
				left: null,
				removed: null,
				role: 'ADMIN',
				groupKey,
				lastUpdated: new Date(),
			},
			{
				groupId: group._id,
				userId: partnerId,
				profileId: partnerProfileId,
				profileKey: partnerProfileKey.data,
				profileKeyIv: partnerProfileKey.iv,
				joined: new Date(),
				left: null,
				removed: null,
				role: 'ADMIN',
				groupKey: partnerGroupKey,
				lastUpdated: new Date(),
			},
		]);

		res.status(200).send(group._id);

		service.getSMQClient()?.publish('TOPIC', `INTERNAL.REFRESH.${userId}`, 'REFRESH');
		service.getSMQClient()?.publish('TOPIC', `INTERNAL.REFRESH.${partnerId}`, 'REFRESH');

		service.getSMQClient()?.publish('TOPIC', `DIRECT.USER.${userId}`, 'GROUP.JOINED', { userId });
		service.getSMQClient()?.publish('TOPIC', `DIRECT.USER.${userId}`, 'GROUP.JOINED', { userId: partnerId });
		service.getSMQClient()?.publish('TOPIC', `DIRECT.USER.${partnerId}`, 'GROUP.JOINED', { userId });
		service.getSMQClient()?.publish('TOPIC', `DIRECT.USER.${partnerId}`, 'GROUP.JOINED', { userId: partnerId });
	} else if (body.type === 'GROUP') {
		const { groupKey, profileId, profileKey, name } = body;
		verifyParameter(groupKey, profileId, profileKey.data, profileKey.iv, name.data, name.iv);

		if (!(await verifyProfileOwnership(userId, profileId))) {
			throw 'error.profile.ownership';
		}

		const group = await GroupDatabase.create({
			type: 'GROUP',
			name: name.data,
			nameIv: name.iv,
		});

		await MembershipDatabase.create({
			groupId: group._id,
			userId,
			profileId,
			profileKey: profileKey.data,
			profileKeyIv: profileKey.iv,
			joined: new Date(),
			left: null,
			removed: null,
			role: 'ADMIN',
			groupKey,
			lastUpdated: new Date(),
		});

		res.status(200).send(group._id);

		service.getSMQClient()?.publish('TOPIC', `INTERNAL.REFRESH.${userId}`, 'REFRESH');
		service.getSMQClient()?.publish('TOPIC', `DIRECT.USER.${userId}`, 'GROUP.JOINED', { userId });
	} else {
		throw 'error.type.invalid';
	}
});

export default router.getExpressRouter();
