import { verifyParameter } from '@kominal/service-util/helper/util';
import MembershipDatabase from '@kominal/connect-models/membership/membership.database';
import Router from '@kominal/service-util/helper/router';
import { getGroup, getMembership } from '../helper';
import service from '..';

const router = new Router();

/**
 * @group Protected
 * @security JWT
 * @route POST /leave
 * @consumes application/json
 * @produces application/json
 * @param {string} group.path.required - The group id
 * @returns {void} 200 - The group was left.
 * @returns {Error}  401 - Unauthorized - Missing authorization header
 * @returns {Error}  406 - Not Acceptable - A parameter is missing or is invalid or the request could not be processed
 * @returns {Error}  500 - Internal Server Error
 */
router.postAsUser('/leave/:groupId', async (req, res, userId) => {
	const { groupId } = req.params;
	verifyParameter(groupId);

	const group = await getGroup(groupId);

	if (group.get('type') === 'DM') {
		throw 'error.group.cannotaddtodm';
	}

	const membership = await getMembership(groupId, userId);

	if (membership.get('left') != null) {
		throw 'error.group.notamember';
	}

	await MembershipDatabase.updateMany({ groupId, userId }, { left: new Date() });

	res.status(200).send();

	service.getSMQClient()?.publish('TOPIC', `DIRECT.GROUP.${group._id}`, 'GROUP.LEFT', { userId });
	service.getSMQClient()?.publish('TOPIC', `INTERNAL.REFRESH.${userId}`, 'REFRESH');
});

export default router.getExpressRouter();
