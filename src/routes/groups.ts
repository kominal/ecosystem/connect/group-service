import MembershipDatabase from '@kominal/connect-models/membership/membership.database';
import GroupDatabase from '@kominal/connect-models/group/group.database';
import { GroupEncrypted } from '@kominal/connect-models/group/group.encrypted';
import Router from '@kominal/service-util/helper/router';
import { AESEncrypted } from '@kominal/connect-models/aes.encrypted';
import { verifyParameter } from '@kominal/service-util/helper/util';
import { getMembership, getGroup } from '../helper';
import service from '..';

const router = new Router();

/**
 * Lists the groups the user is a member of.
 * @group Protected
 * @security JWT
 * @route GET /
 * @consumes application/json
 * @produces application/json
 * @returns {Array} 200 - List of groups
 * @returns {Error}  401 - Unauthorized - Missing authorization header
 * @returns {Error}  406 - Not Acceptable - A parameter is missing or is invalid or the request could not be processed
 * @returns {Error}  500 - Internal Server Error
 */
router.getAsUser('/', async (req, res, userId) => {
	const groups: GroupEncrypted[] = [];

	for (const membership of await MembershipDatabase.find({ userId, removed: null })) {
		const group = await GroupDatabase.findById(membership.get('groupId'));
		if (group == null) {
			continue;
		}

		let name: AESEncrypted | undefined;
		let partnerId: string | undefined;
		let partnerProfileId: string | undefined;
		if (group.get('type') == 'DM') {
			const partner = await MembershipDatabase.findOne({ groupId: group._id, userId: { $ne: userId } });
			if (partner == null) {
				continue;
			}
			partnerId = partner.get('userId');
			partnerProfileId = partner.get('profileId');
		} else {
			name = { data: group.get('name'), iv: group.get('nameIv') };
		}

		groups.push({
			id: group._id,
			name,
			type: group.get('type'),
			userId: partnerId,
			groupKey: membership.get('groupKey'),
			joined: membership.get('joined'),
			left: membership.get('left'),
			role: membership.get('role'),
			lastActivity: group.get('lastActivity'),
			avatarStorageId: group.get('avatarStorageId'),
			lastUpdated: group.get('lastUpdated'),
			partnerId,
			partnerProfileId,
		});
	}

	res.status(200).send(groups);
});

/**
 * Get a specific group
 * @group Protected
 * @security JWT
 * @route GET /{groupId}
 * @consumes application/json
 * @produces application/json
 * @returns {Array} 200 - List of groups
 * @returns {Error}  401 - Unauthorized - Missing authorization header
 * @returns {Error}  406 - Not Acceptable - A parameter is missing or is invalid or the request could not be processed
 * @returns {Error}  500 - Internal Server Error
 */
router.getAsUser('/:groupId', async (req, res, userId) => {
	const { groupId } = req.params;
	verifyParameter(groupId);

	const membership = await getMembership(groupId, userId);

	const group = await GroupDatabase.findById(membership.get('groupId'));
	if (group == null) {
		throw 'error.group.notfound';
	}

	let name: AESEncrypted | undefined;
	let partnerId: string | undefined;
	let partnerProfileId: string | undefined;
	if (group.get('type') == 'DM') {
		const partner = await MembershipDatabase.findOne({ groupId: group._id, userId: { $ne: userId } });
		if (partner == null) {
			throw 'error.partner.notfound';
		}
		partnerId = partner.get('userId');
		partnerProfileId = partner.get('profileId');
	} else {
		name = { data: group.get('name'), iv: group.get('nameIv') };
	}

	res.status(200).send({
		id: group._id,
		name,
		type: group.get('type'),
		userId: partnerId,
		groupKey: membership.get('groupKey'),
		joined: membership.get('joined'),
		left: membership.get('left'),
		role: membership.get('role'),
		lastActivity: group.get('lastActivity'),
		avatarStorageId: group.get('avatarStorageId'),
		partnerId,
		partnerProfileId,
	});
});

/**
 * Update a group
 * @group Protected
 * @security JWT
 * @route POST /{groupId}
 * @consumes application/json
 * @produces application/json
 * @param {string} groupId.path.required - The group id of the group
 * @param {string} name.body.optional - The new name of the group.
 * @returns {void} 200
 * @returns {Error}  401 - Unauthorized - Missing authorization header
 * @returns {Error}  406 - Not Acceptable - A parameter is missing or is invalid or the request could not be processed
 * @returns {Error}  500 - Internal Server Error
 */
router.postAsUser('/:groupId', async (req, res, userId) => {
	const { groupId } = req.params;
	verifyParameter(groupId);

	const membership = await getMembership(groupId, userId);

	if (membership.get('role') !== 'ADMIN') {
		throw 'error.group.notadmin';
	}

	const group = await getGroup(groupId);

	if (group.get('type') === 'DM') {
		throw 'error.group.cannotmodifydm';
	}

	let update: any = {};

	const name = req.body.name;
	if (name && name.data && name.iv) {
		update = { name: name.data, nameIv: name.iv };
	}

	const avatarStorageId = req.body.avatar;
	if (avatarStorageId) {
		update.avatarStorageId = avatarStorageId;
	}

	if (Object.keys(update).length > 0) {
		await GroupDatabase.updateOne({ _id: groupId }, update);
	}

	service.getSMQClient()?.publish('TOPIC', `DIRECT.GROUP.${group._id}`, 'GROUP.UPDATED', { groupId });

	res.status(200).send();
});

export default router.getExpressRouter();
