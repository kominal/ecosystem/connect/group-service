import { verifyParameter } from '@kominal/service-util/helper/util';
import MembershipDatabase from '@kominal/connect-models/membership/membership.database';
import Router from '@kominal/service-util/helper/router';
import { MembershipEncrypted } from '@kominal/connect-models/membership/membership.encrypted';
import { getMembership } from '../helper';
import service from '..';

const router = new Router();

/**
 * Gets a list of members for a group
 * @group Protected
 * @security JWT
 * @route GET /members/{groupId}
 * @consumes application/json
 * @produces application/json
 * @param {string} groupId.path.required - The groupId
 * @returns {Array} 200 - A list of members
 * @returns {Error}  401 - Unauthorized - Missing authorization header
 * @returns {Error}  406 - Not Acceptable - A parameter is missing or is invalid or the request could not be processed
 * @returns {Error}  500 - Internal Server Error
 */
router.getAsRoot('/members/profile/:profileId', async (req, res) => {
	const { profileId } = req.params;
	verifyParameter(profileId);

	const count = await MembershipDatabase.countDocuments({ profileId, left: null });

	res.status(200).send(count > 0);
});

/**
 * Notifies the group service about a profile update
 * @group Protected
 * @security JWT
 * @route GET /members/profile/{profileId}/{lastUpdated}
 * @consumes application/json
 * @produces application/json
 * @param {string} profileId.path.required - The profileId
 * @returns {Array} 200 - The request was consumed
 * @returns {Error}  401 - Unauthorized - Missing authorization header
 * @returns {Error}  406 - Not Acceptable - A parameter is missing or is invalid or the request could not be processed
 * @returns {Error}  500 - Internal Server Error
 */
router.putAsRoot('/members/profile/:profileId/:lastUpdated', async (req, res) => {
	const { profileId, lastUpdated } = req.params;
	verifyParameter(profileId, lastUpdated);

	const memberships = await MembershipDatabase.find({ profileId, left: null });

	for (const membership of memberships) {
		service
			.getSMQClient()
			?.publish('TOPIC', `DIRECT.GROUP.${membership.get('groupId')}`, 'PROFILE.UPDATED', { id: profileId, lastUpdated });
	}

	res.status(200).send();
});

/**
 * Gets a list of members for a group
 * @group Protected
 * @security JWT
 * @route GET /members/{groupId}
 * @consumes application/json
 * @produces application/json
 * @param {string} groupId.path.required - The groupId
 * @returns {Array} 200 - A list of members
 * @returns {Error}  401 - Unauthorized - Missing authorization header
 * @returns {Error}  406 - Not Acceptable - A parameter is missing or is invalid or the request could not be processed
 * @returns {Error}  500 - Internal Server Error
 */
router.getAsUser('/members/:groupId', async (req, res, userId) => {
	const { groupId } = req.params;
	verifyParameter(groupId);

	await getMembership(groupId, userId);

	const members: MembershipEncrypted[] = (await MembershipDatabase.find({ groupId, left: null })).map((membership) => {
		return {
			id: membership._id,
			userId: membership.get('userId'),
			groupId: membership.get('groupId'),
			profileId: membership.get('profileId'),
			profileKey: {
				data: membership.get('profileKey'),
				iv: membership.get('profileKeyIv'),
			},
			joined: membership.get('joined'),
			left: membership.get('left'),
			role: membership.get('role'),
			lastUpdated: membership.get('lastUpdated'),
		};
	});

	res.status(200).send(members);
});

/**
 * Update the profile of a user
 * @group Protected
 * @security JWT
 * @route POST /members/{groupId}
 * @consumes application/json
 * @produces application/json
 * @param {string} groupId.path.required - The groupId
 * @param {string} profileId.body.required - The profileId
 * @param {string} profileKey.body.required - The profileKey
 * @returns {Array} 200 - A new update timestamp
 * @returns {Error}  401 - Unauthorized - Missing authorization header
 * @returns {Error}  406 - Not Acceptable - A parameter is missing or is invalid or the request could not be processed
 * @returns {Error}  500 - Internal Server Error
 */
router.postAsUser('/members/:groupId', async (req, res, userId) => {
	const { groupId } = req.params;
	const { profileId, profileKey } = req.body;
	verifyParameter(groupId, profileId, profileKey?.data, profileKey?.iv);

	const membership = await getMembership(groupId, userId);

	if (membership.get('left') != null) {
		throw 'error.group.notamember';
	}

	const lastUpdated = new Date();

	await MembershipDatabase.updateOne(
		{ groupId, userId },
		{ profileId, profileKey: profileKey.data, profileKeyIv: profileKey.iv, lastUpdated }
	);

	service
		.getSMQClient()
		?.publish('TOPIC', `DIRECT.GROUP.${groupId}`, 'GROUP.PROFILE.CHANGED', { groupId, userId, profileId: profileId, lastUpdated });

	res.status(200).send(lastUpdated);
});

/**
 * @group Private
 * @security TOKEN
 * @route GET /members/{groupId}/current
 * @consumes application/json
 * @produces application/json
 * @param {string} groupId.path.required - The id of the group
 * @returns {void} 200 - A list of current members in a group
 * @returns {Error}  401 - Unauthorized - Missing authorization header
 * @returns {Error}  406 - Not Acceptable - A parameter is missing or is invalid or the request could not be processed
 * @returns {Error}  500 - Internal Server Error
 */
router.getAsRoot('/members/:groupId/current', async (req, res) => {
	const { groupId } = req.params;
	verifyParameter(groupId);

	const members: string[] = [];
	for (const membership of await MembershipDatabase.find({ groupId, left: null })) {
		members.push(membership.get('userId'));
	}

	res.status(200).send(members);
});

/**
 * Returns the membership object of a user in a group.
 * @group Protected
 * @security JWT
 * @route GET /members/{groupId}/time
 * @consumes application/json
 * @produces application/json
 * @param {string} groupId.path.required - The id of the group
 * @returns {void} 200 - The membership object
 * @returns {Error}  401 - Unauthorized - Missing authorization header
 * @returns {Error}  406 - Not Acceptable - A parameter is missing or is invalid or the request could not be processed
 * @returns {Error}  500 - Internal Server Error
 */
router.getAsUser('/members/:groupId/time', async (req, res, userId) => {
	const { groupId } = req.params;
	verifyParameter(groupId);

	const membership = await getMembership(groupId, userId);

	res.status(200).send({
		userId: membership.get('userId'),
		joined: membership.get('joined'),
		left: membership.get('left'),
	});
});

/**
 * Get the membership of a group for a specific user
 * @group Protected
 * @security JWT
 * @route GET /members/{group}/{user}
 * @consumes application/json
 * @produces application/json
 * @param {string} groupId.path.required - The groupId
 * @param {string} partnerId.path.required - The userId of the partner
 * @param {string} lastUpdated.path.optional - The lastUpdated timestamp of the users copy of the membership
 * @returns {void} 200 - TODO
 * @returns {Error}  401 - Unauthorized - Missing authorization header
 * @returns {Error}  406 - Not Acceptable - A parameter is missing or is invalid or the request could not be processed
 * @returns {Error}  500 - Internal Server Error
 */
router.getAsUser('/members/:groupId/:partnerId/:lastUpdated*?', async (req, res, userId) => {
	const { groupId, partnerId, lastUpdated } = req.params;
	verifyParameter(groupId, partnerId);

	await getMembership(groupId, userId);

	const membership = await getMembership(groupId, partnerId);

	if (lastUpdated && membership.get('lastUpdated') && membership.get('lastUpdated').getTime() === Number(lastUpdated)) {
		res.status(304).send();
		return;
	}

	const membershipEncrypted: MembershipEncrypted = {
		id: membership._id,
		userId: membership.get('userId'),
		groupId: membership.get('groupId'),
		profileId: membership.get('profileId'),
		profileKey: {
			data: membership.get('profileKey'),
			iv: membership.get('profileKeyIv'),
		},
		joined: membership.get('joined'),
		left: membership.get('left'),
		role: membership.get('role'),
		lastUpdated: membership.get('lastUpdated'),
	};

	res.status(200).send(membershipEncrypted);
});

export default router.getExpressRouter();
