import { verifyParameter } from '@kominal/service-util/helper/util';
import MembershipDatabase from '@kominal/connect-models/membership/membership.database';
import Router from '@kominal/service-util/helper/router';
import { getGroup, getMembership } from '../helper';
import service from '..';

const router = new Router();

/**
 * Removes a user from a group
 * @group Protected
 * @security JWT
 * @route DELETE /
 * @consumes application/json
 * @produces application/json
 * @param {string} groupId.body.required - The id of the group
 * @param {string} partnerId.body.required - The id of the user to remove
 * @returns {void} 200 - The user was removed
 * @returns {Error}  401 - Unauthorized - Missing authorization header
 * @returns {Error}  406 - Not Acceptable - A parameter is missing or is invalid or the request could not be processed
 * @returns {Error}  500 - Internal Server Error
 */
router.deleteAsUser('/:groupId/:partnerId', async (req, res, userId) => {
	const { groupId, partnerId } = req.params;
	verifyParameter(groupId, partnerId);

	const group = await getGroup(groupId);

	if (group.get('type') === 'DM') {
		throw 'error.group.cannotmodifydm';
	}

	const membership = await getMembership(groupId, userId);

	if (membership.get('role') !== 'ADMIN') {
		throw 'error.group.notadmin';
	}

	const membershipPartner = await getMembership(groupId, partnerId);

	if (membershipPartner.get('left') != null) {
		throw 'error.group.notamember';
	}

	await MembershipDatabase.updateMany({ groupId, userId: partnerId }, { left: new Date() });

	res.status(200).send();

	service.getSMQClient()?.publish('TOPIC', `DIRECT.GROUP.${group._id}`, 'GROUP.LEFT', { partnerId });
	service.getSMQClient()?.publish('TOPIC', `INTERNAL.REFRESH.${partnerId}`, 'REFRESH');
});

export default router.getExpressRouter();
