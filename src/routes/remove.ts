import { verifyParameter } from '@kominal/service-util/helper/util';
import GroupDatabase from '@kominal/connect-models/group/group.database';
import MembershipDatabase from '@kominal/connect-models/membership/membership.database';
import Router from '@kominal/service-util/helper/router';
import axios from 'axios';
import { STACK_NAME, SERVICE_TOKEN, TIMEOUT } from '@kominal/service-util/helper/environment';
import { getGroup, getMembership } from '../helper';
import { error } from '@kominal/service-util/helper/log';
import service from '..';

const router = new Router();

/**
 * @group Protected
 * @security JWT
 * @route DELETE /remove
 * @consumes application/json
 * @produces application/json
 * @param {string} group.path.required - The group id.
 * @returns {void} 200 - The group was deleted.
 * @returns {Error}  401 - Unauthorized - Missing authorization header
 * @returns {Error}  406 - Not Acceptable - A parameter is missing or is invalid or the request could not be processed
 * @returns {Error}  500 - Internal Server Error
 */
router.deleteAsUser('/remove/:groupId', async (req, res, userId) => {
	const { groupId } = req.params;
	verifyParameter(groupId);

	const group = await getGroup(groupId);

	await getMembership(groupId, userId);

	await MembershipDatabase.updateMany({ groupId, userId, left: null }, { left: new Date() });
	await MembershipDatabase.updateMany({ groupId, userId }, { removed: new Date() });

	res.status(200).send();

	service.getSMQClient()?.publish('TOPIC', `DIRECT.GROUP.${group._id}`, 'GROUP.REMOVED', { userId });
	service.getSMQClient()?.publish('TOPIC', `INTERNAL.REFRESH.${userId}`, 'REFRESH');

	const remainingMembers = await MembershipDatabase.find({ groupId, removed: null });

	if (remainingMembers.length == 0) {
		await GroupDatabase.deleteOne({ _id: groupId });
		await MembershipDatabase.deleteMany({ groupId });
		try {
			await axios.delete(`http://${STACK_NAME}_message-service:3000/${groupId}/all`, {
				headers: {
					authorization: SERVICE_TOKEN,
				},
				timeout: TIMEOUT,
			});
		} catch (e) {
			error(e);
		}
	}
});

export default router.getExpressRouter();
