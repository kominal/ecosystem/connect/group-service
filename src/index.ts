import Service from '@kominal/service-util/helper/service';
import add from './routes/add';
import create from './routes/create';
import groups from './routes/groups';
import members from './routes/members';
import remove from './routes/remove';
import leave from './routes/leave';
import role from './routes/role';
import kick from './routes/kick';
import activity from './routes/activity';
import { SMQHandler } from './handler.smq';

const service = new Service({
	id: 'group-service',
	name: 'Group Service',
	description: 'Manages groups as well as the corresponding memberships and permissions.',
	jsonLimit: '16mb',
	routes: [activity, add, create, groups, members, remove, leave, role, kick],
	database: true,
	swarmMQ: new SMQHandler(),
});
service.start();

export default service;
